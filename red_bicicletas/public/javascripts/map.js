//-31.4271999,-64.1820134,16z
//-31.4317097,-64.1701907,19z
//-31.399377,-64.3344309
var mymap = L.map('mapid').setView([-31.4210842,-64.1882361], 15);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1Ijoia21vbmVqdWFuIiwiYSI6ImNrbml6cmQ0ejBhd3Iyd252dGFnamd0eGEifQ.EbSbiSslISHQUnxcSThCGw'
}).addTo(mymap);

L.marker([-31.4265155,-64.1798262]).addTo(mymap);
L.marker([-31.4316583,-64.1702458]).addTo(mymap)